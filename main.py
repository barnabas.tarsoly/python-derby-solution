def read_config_file(filename:str):
    with open(filename) as ff: #forrás megnyitása
        datas = {}
        lines = []
        table, dead_cell_symbol, alive_cell_symbol = False, False, False
        for line in ff:
            line = line.strip()
            if line == "tabla:":
                table = True 
                a = ff.readline()
                if a.strip() != '"': # ha nem idézőjel következik, Error
                    raise AttributeError
                
                while True:
                    a = ff.readline().strip()
                    
                    if a == "": # ha üres sor van, akkor Error
                        raise AttributeError
                    if a == '"': # ha idézőjel van, akkor vége a soroknak
                        
                        break

                    lines.append([x for x in a]) # szétszedi a sorokat listába
            
            elif line.startswith("halott_sejt"):
                dead_cell_symbol = True
                dead = line.split(":")[1].replace(" ", "").replace('"',"").strip() # töri a szóközöket, és az idézőjelet
                
                

                
                if len(dead) > 1 or len(dead) == 0: # ha több karakter van, akkor Error
                    raise AttributeError

            elif line.startswith("elo_sejt"):
                alive_cell_symbol = True
                alive = line.split(":")[1].replace(" ", "").replace('"',"").strip() # töri a szóközöket, és az idézőjelet
               
                if len(alive) >1 or len(alive) == 0: # ha több karakter van, akkor Error
                    raise AttributeError
        if alive_cell_symbol == False or dead_cell_symbol == False or table == False: # ha valami nem található, akkor Error
            raise AttributeError
        line_zero_len = len(lines[0]) 
        for line in lines:
            if len(line) != line_zero_len: # ha valamelyik sor nem egyenlő az első sorra, akkor Error
                raise ValueError
            for symbol in line:
                if symbol == alive or symbol == dead: # ha ismeretlen karakter van, akkor Error
                    pass
                else:
                    raise ValueError

        

        
        datas["halott_sejt"] = dead
        datas["elo_sejt"] = alive
        datas["tabla"] = lines
        
        return datas


config_dict = read_config_file("config_file.config")
print("Kezdő állapot.")
for x in config_dict["tabla"]:
    print(x)

def next_state(config:dict):

    temp_table = []
    table = config["tabla"]
    
    for y, line in enumerate(table):
        temp_line = [x for x in line] 
       
        for x, cell in enumerate(line):
            cell_environment = []
            
            if y == 0:
                if x == 0:
                    cell_environment.append(line[x+1])
                    cell_environment.append(table[y+1][x])
                    cell_environment.append(table[y+1][x+1])

                elif x == len(line)-1: #listába pakolja a környező cellékat, ha legfelül a jobb oldalt található a celle
                    cell_environment.append(table[y][x-1])
                    cell_environment.append(table[y+1][x])
                    cell_environment.append(table[y+1][x-1])
                else: #listába pakolja a környező cellékat, ha legfelül és se jobb se baloldalt van
                    cell_environment.append(line[x-1])
                    cell_environment.append(line[x+1])
                    cell_environment.append(table[y+1][x])
                    cell_environment.append(table[y+1][x+1])
                    cell_environment.append(table[y+1][x-1])
            elif y == len(table)-1:
                if x == 0: #listába pakolja a környező cellékat, ha bal oldalt alul találja
                    cell_environment.append(line[x+1])
                    cell_environment.append(table[y-1][x])
                    cell_environment.append(table[y-1][x+1])
                elif x == len(line)-1: #listába pakolja a környező cellékat, ha jobb oldalt alul találja
                    cell_environment.append(line[x-1])
                    cell_environment.append(table[y-1][x])
                    cell_environment.append(table[y-1][x-1])
                else: #listába pakolja a környező cellékat, ha alul található és se jobb se baloldalt van
                    cell_environment.append(line[x-1])
                    cell_environment.append(line[x+1])
                    cell_environment.append(table[y-1][x])
                    cell_environment.append(table[y-1][x+1])
                    cell_environment.append(table[y-1][x-1])
            else:
                if x == 0: #listába pakolja a környező cellékat, ha bal oldalt van 
                    cell_environment.append(line[x+1])
                    cell_environment.append(table[y-1][x])
                    cell_environment.append(table[y-1][x+1])
                    cell_environment.append(table[y+1][x])
                    cell_environment.append(table[y+1][x+1])
                    
                elif x == len(line)-1: #listába pakolja a környező cellékat, ha jobb oldalt van
                    cell_environment.append(line[x-1])
                    cell_environment.append(table[y-1][x])
                    cell_environment.append(table[y-1][x-1])
                    cell_environment.append(table[y+1][x])
                    cell_environment.append(table[y+1][x-1])

                else: #listába pakolja a környező cellékat, ha nem valamelyik oldalt található
                    cell_environment.append(line[x-1])
                    cell_environment.append(line[x+1])
                    cell_environment.append(table[y-1][x])
                    cell_environment.append(table[y-1][x+1])
                    cell_environment.append(table[y-1][x-1])
                    cell_environment.append(table[y+1][x])
                    cell_environment.append(table[y+1][x+1])
                    cell_environment.append(table[y+1][x-1])
            
            
            if cell == config["halott_sejt"]: # ha halott, akkor megnézi a környező élő sejtek számát
                alive_cells = cell_environment.count(config["elo_sejt"])
                if alive_cells == 3:
                    temp_line[x] = config["elo_sejt"]

            elif cell == config["elo_sejt"]: # ha élő, akkor megnézni a környező sejtek számát
                alive_cells = cell_environment.count(config["elo_sejt"]) #megszámolja az előket
                if alive_cells >= 4 or alive_cells < 2: 
                    temp_line[x] = config["halott_sejt"] # halott lesz, ha megfelel a feltételnek

        temp_table.append(temp_line)
    
    

    new = {"halott_sejt":config["halott_sejt"],"elo_sejt":config["elo_sejt"],"tabla":temp_table}

    return new
final = next_state(config_dict)

print("\nÚj kör:")
for x in final["tabla"]:
    print(x)